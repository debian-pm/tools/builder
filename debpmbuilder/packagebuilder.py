from debpmbuilder.database import Database
from debpmbuilder.docker import Container, Builder
from debpmbuilder.repository import Repository
from debpmbuilder.projectconfig import ProjectConfig
from debpmbuilder.resolver import Resolver
from debpmbuilder.termui import print_ns, print_warn
from debpmbuilder.utils import replace_patterns

import os
import subprocess
import json
import dockerfile_parse

from typing import BinaryIO, List, Dict, Any, Tuple

class PackageBuilder():
    distcc: Container
    build: Container
    repository: Repository

    config: ProjectConfig

    db: Database

    logger: BinaryIO

    def __init__(self, config: ProjectConfig, arch: str = "amd64", logger: BinaryIO = open(os.devnull, 'wb')):
        self.config = config
        self.db = Database(self.config)
        self.logger = logger

        print_ns("package builder", "Building distcc container...")
        builder = Builder(self.config.dockerfiles_dir, "{}/distcc.Dockerfile".format(self.config.dockerfiles_dir), "local/distcc")
        builder.build(stdout=logger)

        print_ns("package builder", "Building emulation container...")
        builder_tag: str = "local/build:testing-{}".format(arch)
        builder_dockerfile_path: str = "{}/package_builder.Dockerfile".format(self.config.dockerfiles_dir)

        parser = dockerfile_parse.DockerfileParser()

        # Read dockerfile
        parser.content = open(builder_dockerfile_path, "r").read()

        # Modify dockerfile
        debian_architectures: Tuple[str, ...] = ("alpha", "amd64", "arm64", "armel", "armhf", "hppa", "i386", "m68k", "mips64el", "mipsel", "powerpcspe", "ppc64", "ppc64el", "riscv64", "s390x", "sh4", "sparc64", "x32")
        parser.baseimage = replace_patterns(parser.baseimage, debian_architectures, arch)

        # Write dockerfile
        builder_tmp_dockerfile_path: str = builder_dockerfile_path + ".tmp"
        open(builder_tmp_dockerfile_path, "w").write(parser.content)

        # Build package builder image
        builder = Builder(self.config.dockerfiles_dir, builder_tmp_dockerfile_path, builder_tag)
        builder.build(stdout=logger)

        # Build distcc image
        self.distcc = Container("local/distcc")
        self.build = Container("local/build:testing-{}".format(arch), mount=(self.db.root_path, "/packages/"))

        subprocess.check_call(["docker", "network", "create", "builder-network"], stdout=logger)
        subprocess.check_call(["docker", "network", "connect", "builder-network", self.distcc.container_name], stdout=logger)
        subprocess.check_call(["docker", "network", "connect", "builder-network", self.build.container_name], stdout=logger)

        # Configure sources list
        echo = subprocess.Popen(["echo", Repository.get_source_line()], stdout=subprocess.PIPE)
        self.build.call(["tee", "-a", "/etc/apt/sources.list.d/local.list"], interactive=True, stdin=echo.stdout, stdout=logger, stderr=logger)
        self.repository = Repository(self.config)
        self.repository.serve()

        # Configure distcc
        self.build.check_call(["dpkg-reconfigure", "distcc"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        wrapper_path: str = "/usr/lib/distcc/distccwrapper"

        # Detect cross-compilers
        compilers_to_replace: List[str] = self.build.check_output(["ls", "/usr/lib/distcc/"]).decode().strip().split("\n")
        compilers_to_replace = [bin for bin in compilers_to_replace if not self.build.check_output(["dpkg-architecture", "-q", "DEB_HOST_GNU_TYPE"]).decode() in bin]

        # Create distcc wrapper
        # It is important that this runs after ls, so the distccwrapper will not be included in compilers_to_replace
        stdin: str = "#!/usr/bin/env bash\n"
        stdin += "/usr/lib/distcc/${DEB_HOST_GNU_TYPE}-g${0:$[-2]} \"$@\"\n"

        stdin_proc = subprocess.Popen(["echo", stdin], stdout=subprocess.PIPE)
        self.build.check_output(["tee", "-a", wrapper_path], interactive=True, stdin=stdin_proc.stdout)

        # Replace cross-compilers with wrapper script
        for bin in compilers_to_replace:
            self.build.check_call(["rm", "/usr/lib/distcc/{}".format(bin)])
            self.build.check_call(["ln", "-s", wrapper_path, "/usr/lib/distcc/{}".format(bin)])

    def __del__(self) -> None:
        try:
            subprocess.call(["docker", "network", "disconnect", "builder-network", self.distcc.container_name])
            subprocess.call(["docker", "network", "disconnect", "builder-network", self.build.container_name])
        except:
            ()

        subprocess.call(["docker", "network", "rm", "builder-network"], stdout=open(os.devnull, "wb"))

    def build_package(self, package: str) -> None:
        package_location: str = "/packages/" + self.db.source_location(package)

        # Get ip of the distcc container
        bridged_containers = json.loads(subprocess.check_output(["docker", "network", "inspect", "builder-network"]).decode())[0]["Containers"]
        self.distcc_ip = ""
        for container in bridged_containers.values():
            if (container["Name"] == self.distcc.container_name):
                self.distcc_ip = container["IPv4Address"].split("/")[0]

        print_ns("package builder", "Building package", package, "in", package_location)
        try:
            if (self.config.package_builder_use_script):
                self.build.check_call(["apt-get", "-qq", "update"], stdout=self.logger)
                self.build.check_call(["/usr/bin/ci-build", "binary"], workdir=package_location, environment={"DISTCC_HOSTS": self.distcc_ip}, stdout=self.logger, stderr=self.logger)
            else:
                common_subprocess_opts: Dict[str, Any] = {
                    "workdir": package_location,
                    "stdout": self.logger,
                    "stderr": self.logger,
                    "environment": { "DISTCC_HOSTS": self.distcc_ip }
                }

                self.build.check_call(["apt-get", "update"], **common_subprocess_opts)
                self.build.check_call(["apt-get", "build-dep", "-y", package_location], **common_subprocess_opts)
                self.build.check_call(["origtargz", "--clean"], **common_subprocess_opts)
                self.build.check_call(["origtargz"], **common_subprocess_opts)
                self.build.check_call(["dpkg-buildpackage"], **common_subprocess_opts)


        except subprocess.CalledProcessError:
            print_warn("Package", package, "failed to build!")
            print_warn("See builder.log for details")
            exit()

        self.repository.scan()

    def build_packages(self, packages: List[str]) -> None:
        for package in packages:
            self.build_package(package)

    def build_package_with_dependencies(self, package: str) -> None:
        resolver = Resolver(self.config)
        buildorder = resolver.srcpkg_buildorder(package)
        print_ns("package builder", "Building packages", buildorder)
        self.build_packages(buildorder)

    def build_packages_with_dependencies(self, packages: List[str]) -> None:
        resolver = Resolver(self.config)
        buildorder = resolver.binpkg_multi_buildorder(packages)
        print_ns("package builder", "Building packages", buildorder)
        self.build_packages(buildorder)
