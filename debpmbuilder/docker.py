import subprocess
import random
import string
import os

from typing import List, Any, Dict, Optional, Tuple

class Builder:
    path: str
    tag: str
    file: str

    def __init__(self, path: str, file: str, tag: str):
        self.path = path
        self.tag = tag
        self.file = file

    def build(self, **kwargs: Any) -> int:
        return subprocess.check_call(["docker", "build", self.path, "-f", self.file, "-t", self.tag], **kwargs)

class Container:
    container_image: str = ""
    container_name: str = ""

    def __init__(self, container_image: str, mount: Optional[Tuple[str, str]] = None, workdir: Optional[str] = None, name: Optional[str] = None):
        self.container_image = container_image

        # Random container name in case none supplied
        if name:
            self.container_name = name
        else:
            self.container_name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))

        docker_extra_args: List[str] = []
        if mount:
            docker_extra_args += ["-v", mount[0] + ":" + mount[1]]

        if workdir:
            docker_extra_args += ["-w", workdir]

        subprocess.check_call(["docker", "run"] + docker_extra_args + ["-t", "-d", "--name", self.container_name, self.container_image], stdout=open(os.devnull, 'wb'))
        subprocess.check_call(["docker", "start", self.container_name], stdout=open(os.devnull, 'wb'))

    def __del__(self) -> None:
        os.system(" ".join(["docker", "stop", self.container_name]))
        os.system(" ".join(["docker", "rm", self.container_name]))

    def __exec_args(self, **kwargs: Any) -> List[str]:
        extra_args: List[str] = []

        # Don't forget to add new handlers in __subprocess_args!
        if "workdir" in kwargs:
            extra_args += ["-w", kwargs["workdir"]]
        if "interactive" in kwargs:
            if kwargs["interactive"]:
                extra_args += ["-i"]
        if "environment" in kwargs:
            for arg in kwargs["environment"].keys():
                extra_args += ["-e", arg + "=" + kwargs["environment"][arg] ]

        return extra_args

    def __subprocess_args(self, **kwargs: Any) -> Dict[str, Any]:
        extra_args = kwargs.copy() # We don't want to edit the original kwargs
        if "workdir" in kwargs:
            extra_args.pop("workdir", None)
        if "interactive" in kwargs:
            extra_args.pop("interactive", None)
        if "environment" in kwargs:
            extra_args.pop("environment", None)

        return extra_args

    def call(self, command: List[str], **kwargs: Any) -> None:
        subprocess.call(["docker", "exec"] + self.__exec_args(**kwargs) + [self.container_name] + command, **self.__subprocess_args(**kwargs))

    def check_output(self, command: List[str], **kwargs: Any) -> bytes:
        output: bytes = subprocess.check_output(["docker", "exec"] + self.__exec_args(**kwargs) + [self.container_name] + command, **self.__subprocess_args(**kwargs))
        return output

    def check_call(self, command: List[str], **kwargs: Any) -> int:
        return subprocess.check_call(["docker", "exec"] + self.__exec_args(**kwargs) + [self.container_name] + command, **self.__subprocess_args(**kwargs))
