from debpmbuilder.database import Database
from debpmbuilder.projectconfig import ProjectConfig

from collections import OrderedDict

from typing import List

class Resolver():
	db: Database

	"""
	Get buildorder for a binary package
	"""
	def binpkg_buildorder(self, bin_pkg: str) -> List[str]:
		if (self.db.binPackageInDb(bin_pkg)):
			deps: List[str] = []
			for dep in self.db.build_deps_of(self.db.source_of_binary(bin_pkg)):
				if (dep != ""):
					deps += self.binpkg_buildorder(dep)

			return list(OrderedDict.fromkeys(deps)) + [self.db.source_of_binary(bin_pkg)]
		else:
			# dependency bin_pkg is not from debian-pm
			return []

	"""
	get buildorder for a source package
	"""
	def srcpkg_buildorder(self, source_pkg: str) -> List[str]:
		buildorder: List[str] = []
		for dep in self.db.build_deps_of(source_pkg):
			buildorder += self.binpkg_buildorder(dep)

		return list(OrderedDict.fromkeys(buildorder)) + [source_pkg]

	def __init__(self, config: ProjectConfig):
		self.db = Database(config)

	"""
	Resolves a build order for multiple binary packages
	"""
	def binpkg_multi_buildorder(self, packages: List[str]) -> List[str]:
		buildorder: List[str] = []

		for package in packages:
			buildorder += self.binpkg_buildorder(package)

		return list(OrderedDict.fromkeys(buildorder))
