from debpmbuilder.projectconfig import ProjectConfig
from debpmbuilder.simpleyaml import SimpleYaml
from debpmbuilder.docker import Builder
from debpmbuilder.repository import Repository
from debpmbuilder.utils import dirname, relative_path
from debpmbuilder.termui import print_err

import os
import subprocess
import json
import gtmpl
import ruamel.yaml

from typing import Dict, List, Any, BinaryIO, Union

class DebosRecipe():
    __yaml: SimpleYaml

    recipe: Dict[str, Any]
    root_path: str
    path: str
    variables: Dict[str, Any]
    config: ProjectConfig
    logger: BinaryIO

    def __init__(self, config: ProjectConfig, path: str, logger: BinaryIO = open(os.devnull, 'wb'), **kwargs: str) -> None:
        self.config = config
        self.root_path = dirname(path)
        self.path = path
        self.__yaml = SimpleYaml()
        self.logger = logger

        # Store recipe expanded
        self.recipe = self.__expand_recipe(path, **kwargs)
        self.variables = kwargs

    """
    returns the content of the recipe at the path
    """
    def __parse_recipe(self, path: str, **kwargs: str) -> Dict[str, Any]:
        try:
            recipe: Dict[str, Any] = self.__yaml.load(gtmpl.render_template(open(path).read(), **kwargs))
            return recipe
        except ValueError as e:
            exit("Failed to parse recipe template: {}".format(e))
        except ruamel.yaml.parser.ParserError as e:
            exit("Failed to parse recipe: {}".format(e))

    """
    returns the content of the recipe and any included recipes
    """
    def __expand_recipe(self, path: str, **kwargs: str) -> Dict[str, Any]:
        recipe: Dict[str, Any] = self.__parse_recipe(path, **kwargs)

        current_dir: str = dirname(path)

        # Copy unexpanded recipe and reset actions
        expanded_recipe: Dict[str, Any] = recipe.copy()
        expanded_recipe["actions"] = []

        # Add actions
        for a in recipe["actions"]:
            if a["action"] == "recipe":
                variables: Dict[str, Any] = a["variables"]

                # Pass down architecture variable
                variables["architecture"] = recipe["architecture"]

                expanded_recipe["actions"] += (self.__expand_recipe(current_dir + "/" + a["recipe"], **variables)["actions"])
            elif a["action"] == "run" and "script" in a:
                # resolve all script paths to be relative to the directory the root recipe is in
                expanded_recipe["actions"].append(a)
                expanded_recipe["actions"][-1]["script"] = relative_path(current_dir + "/" + a["script"], self.root_path)
            else:
                expanded_recipe["actions"].append(a)

        return expanded_recipe

    def get_packages(self) -> List[str]:
        # Expand recipe to get even packages from included recipes
        recipe = self.__expand_recipe(self.path, **self.variables)

        packages: List[str] = []

        for a in recipe["actions"]:
            if (a["action"] == "apt"):
                packages += a["packages"]

        return packages

    def write_tmp_file(self) -> str:
        path: str = self.root_path + "/.tmp-debos-recipe.yml"
        file = open(path, "w")
        self.__yaml.dump(self.recipe, file)

        file.flush()
        file.close()

        return path.replace(self.root_path + "/", "")

    def inject_repo_setup(self) -> bool:
        # Make sure not already done
        for a in self.recipe["actions"]:
            if (a["action"] == "run"):
                try:
                    if (Repository.get_source_line() in a["command"]):
                        # Already exists
                        return False
                except:
                    continue

        repo_add_action: Dict[str, Union[str, bool]] = {
            "action": "run",
            "chroot": True,
            "command": "echo {} >/etc/apt/sources.list.d/local.list".format(Repository.get_source_line())
        }

        repo_remove_action: Dict[str, Union[str, bool]] = {
            "action": "run",
            "chroot": True,
            "command": "rm /etc/apt/sources.list.d/local.list"
        }

        # Inject before first apt action
        for i in range(len(self.recipe["actions"]) - 1):
            if (self.recipe["actions"][i]["action"] == "apt" or self.recipe["actions"][i]["action"] == "recipe"):
                self.recipe["actions"].insert(i - 1, repo_add_action)
                break

        last_apt_action_index: int = 0
        for i in range(len(self.recipe["actions"]) - 1):
            if (self.recipe["actions"][i]["action"] == "apt" or self.recipe["actions"][i]["action"] == "recipe"):
                last_apt_action_index = i

        self.recipe["actions"].insert(last_apt_action_index + 1, repo_remove_action)

        return True

    def build(self) -> None:
        # print expanded recipe for debugging
        self.logger.write(json.dumps(self.recipe, indent=4).encode())

        recipe_path: str = self.write_tmp_file()

        builder = Builder(self.config.dockerfiles_dir, "{}/debos.Dockerfile".format(self.config.dockerfiles_dir), "debian-pm/debos")
        builder.build()

        repository = Repository(self.config)
        repository.serve()

        user = subprocess.check_output(["id", "-u"]).decode().strip()
        group = subprocess.check_output("getent group kvm | cut -d : -f 3", shell=True).decode().strip()

        # Note: There is no need to pass template variables to debos, as the top level recipe is already rendered by gtmpl,
        # 		and the sub recipe actions don't inherit template variables anyway.
        try:
            subprocess.check_call(
                ["docker", "run", "--net=host", "--rm", "--interactive", "--tty", "--device", "/dev/kvm", "--user", user, "--workdir", "/recipes",
                "--group-add={}".format(group), "--mount", "type=bind,source={},destination=/recipes".format(self.root_path),
                "--security-opt", "label=disable", builder.tag, "--memory=4G", recipe_path], shell=False, cwd=self.root_path)
        except subprocess.CalledProcessError:
            print_err("Failed to build the preprocessed recipe in debos. If the recipe works fine in regular debos, please report a bug against this tool after checking {} is correct.".format(recipe_path))
