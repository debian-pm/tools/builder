#!/usr/bin/env python3

import json
import argparse
import time
import subprocess
import pathlib
import os

from debpmbuilder.projectconfig import ProjectConfig
from debpmbuilder.simpleyaml import SimpleYaml
from debpmbuilder.database import Database
from debpmbuilder.repository import Repository
from debpmbuilder.packagebuilder import PackageBuilder
from debpmbuilder.resolver import Resolver
from debpmbuilder.debosrecipe import DebosRecipe
from debpmbuilder.termui import print_warn, print_err
from debpmbuilder.docker import Container, Builder

from typing import BinaryIO, Dict, Any

def main() -> None:
    parser = argparse.ArgumentParser(description="Debian system image builder")
    subparsers: argparse._SubParsersAction = parser.add_subparsers(dest="subcommand", help='sub help')

    parser_install: argparse.ArgumentParser = subparsers.add_parser("install", help="Install to image")
    parser_build: argparse.ArgumentParser = subparsers.add_parser("build", help="Build packages")
    parser_new_package: argparse.ArgumentParser = subparsers.add_parser("new-package", help="Create a new package")
    parser_exec: argparse.ArgumentParser = subparsers.add_parser("exec", help="Execute a command in the build container")

    # Shared opts
    parser.add_argument("--arch", type=str,
                        help="Architecture to build for",
                        default="armhf")

    # Install mode opts
    parser_install.add_argument("-t",
                        action="append",
                        help="variable for image template key:value")
    parser_install.add_argument("--recipe", type=str, required=True,
                        help="Recipe to build")
    parser_install.add_argument("--build-dependencies", action="store_true",
                        help="Build packages used by the recipe before installing")

    # Build mode opts
    parser_build.add_argument("--package", type=str, required=True,
                        help="Package to be built")
    parser_build.add_argument("--include-dependencies", action="store_true",
                        help="Build dependencies before building a package")

    # New package opts
    parser_new_package.add_argument("--path", type=str,
                        help="path relative to packages_dir")

    # Exec args
    parser_exec.add_argument("--command", nargs="+", type=str, required=True,
                        help="Command to run in the container. The command is executed in the current directory")

    # Initialization
    args: argparse.Namespace = parser.parse_args()

    logger: BinaryIO = open("builder.log", "wb")

    settings: ProjectConfig = ProjectConfig()

    try:
        # Build subcommand
        if (args.subcommand == "build"):
            builder = PackageBuilder(settings, logger=logger, arch=args.arch)

            if args.include_dependencies:
                builder.build_package_with_dependencies(args.package)
            else:
                builder.build_package(args.package)

        # Install subcommand
        elif (args.subcommand == "install"):
            if not args.recipe:
                exit("cannot proceed without --recipe")

            kwargs: Dict[str, Any] = {}

            # Pass arch argument to recipe template renderer
            if (args.arch):
                kwargs["architecture"] = args.arch

            if args.t:
                for var in args.t:
                    key, value = var.split(":")
                    kwargs[key] = value

            debos = DebosRecipe(settings, args.recipe, logger, **kwargs)

            if args.build_dependencies:
                # Build packages for the architecture of the recipe
                builder = PackageBuilder(settings, logger=logger, arch=debos.recipe["architecture"])

                # Build packages used in the recipe
                builder.build_packages_with_dependencies(debos.get_packages())

            # Add repository
            debos.inject_repo_setup()

            debos.build()

        elif (args.subcommand == "new-package"):
            try:
                # Python doesn't seem to support "cond ? 1 : 2" as function arg,
                # so waste a variable here
                relative_path: str = ""
                if args.path:
                    relative_path = args.path

                cwd_path = "{}/{}".format(settings.packages_dir, relative_path)
                pathlib.Path(cwd_path).mkdir(parents=True, exist_ok=True)
                subprocess.call(["package-generator"], cwd=cwd_path)
            except FileNotFoundError:
                print_err("Please install the package-generator binary and try again")
        elif (args.subcommand == "exec"):

            image_builder = Builder(settings.dockerfiles_dir,
                "{}/package_builder.Dockerfile".format(settings.dockerfiles_dir),
                "local/build:testing")
            image_builder.build()

            current_dir: str = os.getcwd()
            container: Container = Container("local/build:testing",
                mount=(current_dir, current_dir),
                workdir=current_dir
            )
            container.call(args.command)
        else:
            parser.print_help()

    except KeyboardInterrupt:
        print("Cleaning up running containers...")
        print_warn("Pressing Ctrl + C again might leave containers in an unclean state!")
        exit()

if __name__ == "__main__":
    main()
