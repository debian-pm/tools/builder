from ruamel.yaml import YAML, comments
from ruamel.yaml.compat import StringIO

from typing import Any

class SimpleYaml(YAML):
    def dumps(self, data: Any, **kwargs: Any) -> str:
        stream: StringIO = StringIO()
        YAML.dump(self, data, stream, **kwargs)
        yaml_text: str = stream.getvalue()
        return yaml_text
