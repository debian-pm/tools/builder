import colorful as cf

from typing import Any

def print_ns(component: str, *args: Any) -> None:
    print("{}:".format(cf.bold_white(component)), *args)

def print_warn(*args: Any) -> None:
    print(cf.yellow("Warning:"), *args)

def print_err(*args: Any) -> None:
    print(cf.red("Error:"), *args)
