from debian.deb822 import Sources, PkgRelation, Packages, Deb822, Deb822Dict
import os

from debpmbuilder.projectconfig import ProjectConfig

from typing import Dict, List, TextIO

class Database():
	root_path: str

	pkg_list_file: TextIO

	dependencies_dict: Dict[str, List[str]] = {}
	bin_source_dict: Dict[str, str] = {}
	location_dict: Dict[str, str] = {}

	def source_location(self, srcpkg: str) -> str:
		return self.location_dict[srcpkg]

	def source_of_binary(self, binpkg: str) -> str:
		return self.bin_source_dict[binpkg]

	def build_deps_of(self, srcpkg: str) -> List[str]:
		try:
			return self.dependencies_dict[srcpkg]
		except KeyError:
			exit("Package {} not found".format(srcpkg))

	def binPackageInDb(self, package: str) -> bool:
		return package in self.bin_source_dict

	def load_packages(self) -> None:
		# iterate over list
		for line in self.pkg_list_file.read().split("\n"):
			if not line.startswith("#") and line != "":
				# read packaging
				controlFilePath = self.root_path + "/" + line.strip() + "/debian/control"
				source = Sources(open(controlFilePath).read())

				# Store location of the package
				self.location_dict[source["Source"]] = line.strip()

				self.dependencies_dict[source["Source"]] = []
				for dep in source.relations["build-depends"]:
					self.dependencies_dict[source["Source"]].append(dep[0]["name"])

				packages = Packages.iter_paragraphs(open(controlFilePath))
				for package in packages:
					try:
						self.bin_source_dict[Deb822Dict(package)["Package"]] = source["Source"]
					except:
						continue # Skip sources

	def __init__(self, config: ProjectConfig):
		self.root_path = "{}/{}".format(os.getcwd(), config.packages_dir)

		self.pkg_list_file = open("{}/packages.list".format(self.root_path))

		self.load_packages()
