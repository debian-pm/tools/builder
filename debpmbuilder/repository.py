import _thread
import subprocess
import os

from debpmbuilder.projectconfig import ProjectConfig
from debpmbuilder.termui import print_err

from typing import Any

class Repository():
    root_path: str
    http_server: subprocess.Popen

    def scan(self) -> None:
        try:
            subprocess.check_call(["package-scanner", "-b", self.root_path])
        except FileNotFoundError:
            print_err("Please install the package-scanner binary and try again")
            exit(1)

    @staticmethod
    def get_source_line() -> str:
        return "deb [trusted=yes] http://172.17.0.1:8000/ ./"

    def serve(self) -> None:
        # 172.17.0.1 is the docker host ip address
        self.http_server = subprocess.Popen(["python3", "-m", "http.server", "-d", self.root_path, "--bind", "172.17.0.1"], stdout=None, stderr=None)

        # Nicer way, but subprocess was easier to get running in the right directory
        #server_address = ('0.0.0.0', 8000)
        #self.http_server = HTTPServer(server_address, SimpleHTTPRequestHandler)
        #_thread.start_new_thread(self.http_server.serve_forever, tuple())

    def stop_server(self) -> None:
        print("Stopping server")
        self.http_server.kill()

    def __init__(self, config: ProjectConfig) -> None:
        self.root_path = "{}/{}".format(os.getcwd(), config.packages_dir)
        self.scan()

    def __del__(self) -> None:
        self.stop_server()
