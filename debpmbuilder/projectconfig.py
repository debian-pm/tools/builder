from debpmbuilder.simpleyaml import SimpleYaml
from debpmbuilder.termui import print_err

from ruamel.yaml import YAML, yaml_object, scanner

yaml = YAML()

class ProjectConfig(object):
	dockerfiles_dir: str = "docker/"
	debos_dir: str = "recipes/"
	packages_dir: str = "packages/"
	package_builder_use_script: bool = False

	def __init__(self) -> None:
		try:
			self.__dict__ = yaml.load(open("project.yaml"))

		except FileNotFoundError:
			print("No project configuration found in project.yaml, using defaults")
		except scanner.ScannerError as e:
			print_err("Failed to parse project configuration: {}".format(e))
			exit(1)
