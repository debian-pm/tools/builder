import subprocess

from typing import Tuple

"""
returns the absolute path of the file
"""
def readlink(path: str) -> str:
	return subprocess.check_output(["readlink", "-f", path]).decode().strip()

"""
returns the absolute path of the directory a file is in
"""
def dirname(path: str) -> str:
	return readlink(subprocess.check_output(["dirname", path]).decode().strip())

"""
returns the path relative to another path
"""
def relative_path(path: str, to: str) -> str:
	return "." + readlink(path).replace(readlink(to), "")

"""
replace multiple patterns in a string with one string
"""
def replace_patterns(input: str, patterns: Tuple[str, ...], replacement: str) -> str:
	if (len(patterns) == 0):
		return input

	return replace_patterns(input.replace(patterns[0], replacement), patterns[1:], replacement)
