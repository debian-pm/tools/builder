#!/usr/bin/env bash

# source: https://source.puri.sm/Librem5/image-builder/blob/master/conf/board
kernel_latest_url='https://arm01.puri.sm/job/Kernel_builds/job/devkit_kernel_build/lastSuccessfulBuild/artifact/*zip*/archive.zip'
uboot_latest_url='https://arm01.puri.sm/job/u-boot_builds/job/devkit_test_uboot_build/lastSuccessfulBuild/artifact/*zip*/archive.zip'

echo "Installing wget for downloading packages from Purism..."
apt install wget -y

echo "Fetching kernel from ${kernel_latest_url} ..."
wget -nv --no-glob -P /scratch ${kernel_latest_url}
(cd /scratch && unzip -ojq archive.zip)

for deb in scratch/linux-image-*-*_arm64.deb; do
    ! [[ "${deb}" =~ dbg ]] || continue
    if [ -n "${kernel_deb}" ]; then
        echo "Found more than one possible kernel deb in files/"
        exit 1
    fi
    kernel_deb="$deb"
done

echo "Installing kernel"
dpkg -i ${kernel_deb}
