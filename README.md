This project builds operating system images of debian based distributions from packaging sources and debos recipes.
A typical build process first builds the containers to build in, then the packages and finally runs debos to assemble the image.
The aim is to only use existing file formats and for project files to stay compatible with the tools used internally.
For example a debos recipe for this tool can also be built with standalone debos, but then debos will try to fetch the packages from a configured remote repository instead the local one managed by this tool.
To extract the information it needs from the debos recipe, bauleiter renders the template and resolves all includes.
Afterwards it makes its changes related to local packages to the recipe and serializes a yaml file for debos again.

# Installation
```
pip3 install --extra-index-url https://archive.kaidan.im/debian-pm/wheels -r requirements.txt --force
sudo python3 setup.py install
```

If you prefer not to use the prebuilt binary wheels, you can build them yourself.
This requires a rust nightly toolchain, that's why prebuilt wheels are available.
```
pip3 install maturin
git clone --recursive https://gitlab.com/debian-pm/tools/debian-pm-cli-tools
cd debian-pm-cli-tools
./build_wheels.sh
cd wheels
python3 -m http.server
```
Afterwards you can follow the installation instructions above, but with archive.kaidan.im replaced by http://0.0.0.0:8000/.

# Project structure

A project typically consists of the following structure.

```
/
/project.yaml
/recipes/
/recipes/recipe.yaml
/packages/
/packages/packages.list
/packages/example-package/
/docker/
/docker/package_builder.Dockerfile
/docker/distcc.Dockerfile
/docker/debos.Dockerfile
```
An example is provided for easier understanding.

## docker/package_builder.Dockerfile

### FROM
The FROM line needs to contain an architecture, e.g amd64 that can be automatically replaced to build for another architecture.

example dockerfile:
```
FROM registry.gitlab.com/debian-pm/tools/build:testing-amd64
```
is replaced with:
```
FROM registry.gitlab.com/debian-pm/tools/build:testing-armhf
```
if you pass a `--arch armhf` argument to debpmbuilder

### Executables
The docker container needs to contain a special executable file, `/usr/bin/ci-build`, which is responsible for installing build dependencies and executing dpkg-buildpackage.

A simple script would be
```
apt build-dep . -y
dpkg-buildpackage
```

In most cases, the script should also setup distcc. A default `${DISTCC_HOSTS}` environment variable is passed by debpmbuilder.
For cross-compiling, some additional code might be required in the docker container.
See line 114 to 129 of [this script](https://gitlab.com/debian-pm/tools/build/blob/master/docker/ci-build.sh#L114) for an example.

## Command line arguments

`debpmbuilder` accepts the following positional arguments: `build`, `ìnstall` and `new-package`. It also accepts the `--arch` argument which is currently used by `build` and `install`.
The positional arguments accept their own options, see `debpmbuilder <positional> --help}`.

## Project configuration file
The configuration file is located in the root of the project, as `project.yaml`.
The following keys are supported:
```
string dockerfiles_dir
string debos_dir
string packages_dir
bool   package_builder_use_script
```
